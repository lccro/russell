require_relative 'logic'

class Sudoku
  col_thirds = [%w{A B C}, %w{D E F}, %w{G H I}]
  row_thirds = [%w{1 2 3}, %w{4 5 6}, %w{7 8 9}]

  @@boxes = row_thirds.product(col_thirds).map {|set| set[1].product(set[0])}
            .map { |box| box.map { |combo| combo.join.to_sym } }

  col_signs = col_thirds.flatten
  row_signs = row_thirds.flatten

  @@cells = row_signs.product(col_signs).map { |combo| combo.reverse.join.to_sym }
  @@cols = @@cells.each_slice(9).to_a
  @@rows = @@cols.transpose

  @@units = @@cols + @@rows + @@boxes

  def parse_grid(str)
    @@cells.zip(str.chars)
      .keep_if { |cell, char| /[[:digit:]]/ === char }
      .map { |cell, char| [cell, char.to_i] }
  end

  def initialize(str)
    starting_grid = parse_grid(str)
    solver = Logic::Solver.new do
      @@cells.each { |cell| assert domain_is( cell, (1..9) ) }

      @@units.each do |unit|
        assert has_unique_contents(unit)
        (1..9).each { |num| assert must_include(unit, num) }
      end
      starting_grid.each { |cell, value| assert equal(cell, value) }
    end
    solver.solve
    @results = solver.results
  end

  def to_s
    @results.map do |result|
      @@cells.map { |cell| result[cell].to_s }
        .each_slice(9)
        .map(&:join)
    end.join("\n")
  end
end
