module Logic
  module Util
    def self.deep_copy(obj)
      Marshal.load(Marshal.dump(obj))
    end

    def self.product(arrs)
      arrs[0].product(*arrs[1..-1])
    end
  end
end
